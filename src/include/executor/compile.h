/* We need the ifdef because we can't use 'extern "C"' in a C file, only C++ */

#ifdef __cplusplus
extern "C"
#endif
int compile();

#ifdef __cplusplus
extern "C"
#endif
bool ExecuteCompiledPlan(EState *estate,
                         PlanState *planstate,
                         bool use_parallel_mode,
                         CmdType operation,
                         bool sendTuples,
                         uint64 numberTuples,
                         ScanDirection direction,
                         DestReceiver *dest,
                         bool execute_once);

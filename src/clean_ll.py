import sys
import re

def delete_struct(lines, s):
  #return [line for line in lines if s + ' = ' not in line]
  return [line for line in lines if not re.search(s+'.[0-9]+ =', line)]

def replace_struct(lines, s):
  pattern = s + '\.[0-9]+'
  return [re.sub(pattern, s, line) for line in lines]

def delete_func(lines, f):
  filt = [True] * len(lines)
  remove = False
  for i in range(len(lines)):
    line = lines[i]
    if 'define' in line and f in line:
      remove = True
    if remove:
      filt[i] = False
    if '}\n' == line:
      remove = False
  return [i for i,j in zip(lines,filt) if j == True]


# removes struct.[0-9]+
structs = ['struct.PlanState', 'struct.EState', 'struct.ExprState', 'struct.ExprContext', 'struct.ProjectionInfo']
# replace func.[0-9]+ for func
funcs_replace = ['ExecProcNode']
# remove func
funcs_delete = ['ExecInitSeqScan', 'ExecInitNestLoop', 'ExecInitAgg', 'ExecInitMaterial', 'ExecInitHashJoin', 'ExecInitNode', 'ExecInitHash', 'ExecInitSort']
funcs_delete += ['InitPlan', 'EvalPlanQualStart', 'EvalPlanQualBegin', 'EvalPlanQual', 'standard_ExecutorStart', 'ExecutorStart']

if len(sys.argv) != 3:
  print sys.argv[0] + ' <input.ll> <output.ll>'
  sys.exit(0)

with open(sys.argv[1]) as f:
  lines = f.readlines()
  for struct in structs:
    lines = delete_struct(lines, struct)
    lines = replace_struct(lines, struct)
  for func in funcs_replace:
    lines = delete_func(lines, func+'.')
    lines = replace_struct(lines, func)
  for func in funcs_delete:
    lines = delete_func(lines, func)

with open(sys.argv[2], 'w') as fnew:
  fnew.write(''.join(lines))

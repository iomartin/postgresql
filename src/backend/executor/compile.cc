#include "postgres.h"

// Needed by ExecuteCompiledPlan
#include "foreign/fdwapi.h"
#include "tcop/utility.h"

// for timing
#include "portability/instr_time.h"

#include "executor/compile.h"
#include "executor/executor.h"
#include "executor/KaleidoscopeJIT.h"
#include "executor/nodeSeqscan.h"
#include "executor/nodeNestloop.h"
#include "executor/nodeAgg.h"
#include "executor/nodeMaterial.h"
#include "executor/nodeHashjoin.h"
#include "executor/nodeSort.h"
#include "executor/nodeGather.h"
#include "executor/nodeIndexscan.h"
#include "executor/nodeGatherMerge.h"
#include "executor/nodeBitmapHeapscan.h"
#include "executor/nodeBitmapIndexscan.h"
#include "executor/nodeMergejoin.h"
#include "executor/nodeIndexonlyscan.h"
#include "executor/nodeSubqueryscan.h"
#include "executor/nodeLimit.h"
#include "executor/nodeResult.h"

#include <llvm/ADT/ArrayRef.h>

#include <llvm/ExecutionEngine/GenericValue.h>

#include <llvm/IR/InstIterator.h>
#include <llvm/IR/LLVMContext.h>
#include <llvm/IR/Module.h>
#include <llvm/IR/Verifier.h>

#include <llvm/IRReader/IRReader.h>

#include <llvm/Support/raw_ostream.h>
#include <llvm/Support/SourceMgr.h>
#include <llvm/Support/TargetSelect.h>
#include <llvm/Transforms/Utils/Cloning.h>
#include <llvm/Transforms/Utils/BasicBlockUtils.h>

#include <sstream>

#include <mutex>
std::mutex mtx;

using namespace llvm;
using namespace llvm::orc;

// X ExecAgg
//   ExecAppend
//   ExecBitmapAnd
// X ExecBitmapHeapScan
//   ExecBitmapIndexScan
//   ExecBitmapOr
//   ExecCteScan
//   ExecCustomScan
//   ExecForeignScan
//   ExecFunctionScan
// X ExecGather
// X ExecGatherMerge
//   ExecGroup
//   ExecHash
// X ExecHashJoin
// O ExecIndexOnlyScan
// O ExecIndexScan
// X ExecLimit
//   ExecLockRows
// X ExecMaterial
//   ExecMergeAppend
// X ExecMergeJoin
//   ExecModifyTable
//   ExecNamedTuplestoreScan
// X ExecNestLoop
//   ExecProcNodeFirst
//   ExecProcNodeInstr
//   ExecProjectSet
//   ExecRecursiveUnion
// X ExecResult
//   ExecSampleScan
// O ExecSeqScan
//   ExecSetOp
// X ExecSort
// O ExecSubqueryScan
//   ExecTableFuncScan
//   ExecTidScan
//   ExecUnique
//   ExecValuesScan
//   ExecWindowAgg
//   ExecWorkTableScan

// X MultiExecHash
// O MultiExecBitmapIndexScan
//   MultiExecBitmapAnd
//   MultiExecBitmapOr

static Function* patchExecProcNode(std::unique_ptr<Module> &Owner, PlanState *node);
static Function* patchMultiExecProcNode(std::unique_ptr<Module> &Owner, PlanState *node);

static Function *cloneFunc(std::unique_ptr<Module> &Owner, std::string Name)
{
  Function *OriginalFunc = Owner->getFunction(Name);
  ValueToValueMapTy VMap;
  Function *SpecializedFunc = CloneFunction(OriginalFunc, VMap);
  SpecializedFunc->setName(Name + "JIT");
  errs() << *OriginalFunc << "\n";
  return SpecializedFunc;
}

/* Because ExecProcNode is an indirect function call and a special one, we make this a special case for the more general
   findCallsToFunc function */
std::vector<CallInst*> findCallsToExecProcNode(std::unique_ptr<Module> &Owner, Function *Caller, int num=1)
{
  int found = 0;
  std::vector<CallInst*> CallInsts;

  PointerType *TTS = Owner->getTypeByName("struct.TupleTableSlot")->getPointerTo();
  PointerType *PS = Owner->getTypeByName("struct.PlanState")->getPointerTo();
  if (!TTS || !PS) {
    errs() << "Could not find TTS (" << TTS << ") or PS (" << PS << ")\n";
    return {};
  }

  for (inst_iterator I = inst_begin(Caller), E = inst_end(Caller); I != E && found < num; I++) {
    if (CallInst *C = dyn_cast<CallInst>(&*I)) {
      /* it is an indirect call, so getCalledFunction() should be null */
      if (C->getCalledFunction() == nullptr) {
        FunctionType *FT = C->getFunctionType();
        if (FT->getReturnType() != TTS)
          continue;
        if (FT->getNumParams() != 1 || FT->getParamType(0) != PS)
          continue;
        CallInsts.push_back(C);
        found++;
      }
    }
  }

  if (found != num) {
    errs() << "[WARNING] findCallsToExecProcNode: found " << found << " CallInst to ExecProcNode (requested: " << num << ")\n";
    return {};
  }

  return CallInsts;
}

/* Finds the first "num" calls to function "CalleeName" within function "Caller" */
std::vector<CallInst*> findCallsToFunc(std::unique_ptr<Module> &Owner, Function *Caller, std::string CalleeName, int num=1)
{
  if (CalleeName == "ExecProcNode")
    return findCallsToExecProcNode(Owner, Caller, num);

  std::vector<CallInst*> CallInsts;
  Function *Callee = Owner->getFunction(CalleeName);

  int found = 0;

  for (inst_iterator I = inst_begin(Caller), E = inst_end(Caller); I != E && found < num; I++) {
    if (CallInst *C = dyn_cast<CallInst>(&*I)) {
      if (C->getCalledFunction() == Callee) {
        CallInsts.push_back(C);
        found++;
      }
    }
  }

  if (found != num) {
    errs() << "[WARNING] findCallsToFunc: found " << found << " CallInst to " << CalleeName << " (requested: " << num << ")\n";
    return {};
  }

  return CallInsts;
}

void createNewCall(CallInst *OldCallInst, Function *NewFunc)
{
  CallSite CS(OldCallInst);
  SmallVector<llvm::Value *, 8> Args(CS.arg_begin(), CS.arg_end());
  CallInst *NewCI = CallInst::Create(NewFunc, Args);
  NewCI->setCallingConv(NewFunc->getCallingConv());
  if (!OldCallInst->use_empty())
    OldCallInst->replaceAllUsesWith(NewCI);
  ReplaceInstWithInst(OldCallInst, NewCI);
}

/* casts first parameter to TypeTo */
void createNewCallCast(CallInst *OldCallInst, Function *NewFunc, Type *TypeTo)
{
  CastInst *CI = CastInst::CreatePointerCast(OldCallInst->getArgOperand(0),
                                             TypeTo,
                                             "",
                                             OldCallInst);
  CallSite CS(OldCallInst);
  SmallVector<llvm::Value *, 1> Args;
  Args.push_back(CI);
  CallInst *NewCI = CallInst::Create(NewFunc, Args);
  NewCI->setCallingConv(NewFunc->getCallingConv());
  if (!OldCallInst->use_empty())
    OldCallInst->replaceAllUsesWith(NewCI);
  ReplaceInstWithInst(OldCallInst, NewCI);
}

static Function* patchFetchInputTuple(std::unique_ptr<Module> &Owner, PlanState *pstate)
{
  Function *SpecializedFunc = cloneFunc(Owner, "fetch_input_tuple");

  std::vector<CallInst*> CallInsts = findCallsToFunc(Owner, SpecializedFunc, "ExecProcNode");
  if (CallInsts.size() != 1)
    return nullptr;

  AggState *aggstate = castNode(AggState, pstate);
  Function *ExecProcNodeSpecialized = patchExecProcNode(Owner, outerPlanState(aggstate));
  if (!ExecProcNodeSpecialized)
    return nullptr;
  createNewCall(CallInsts[0], ExecProcNodeSpecialized);

  if(verifyFunction(*SpecializedFunc, &errs())) {
    errs() << "Error specializing function fetch_input_tuple\n";
    errs() << *SpecializedFunc;
    return nullptr;
  }

  return SpecializedFunc;
}

static Function* patchExecAgg(std::unique_ptr<Module> &Owner, PlanState *pstate)
{
  Function *SpecializedFunc = cloneFunc(Owner, "ExecAgg");

  std::vector<CallInst*> CallInsts = findCallsToFunc(Owner, SpecializedFunc, "fetch_input_tuple", 4);
  if (CallInsts.size() != 4)
    return nullptr;

  Function *FetchInputTupleSpecialized = patchFetchInputTuple(Owner, pstate);
  if (!FetchInputTupleSpecialized)
    return nullptr;

  createNewCall(CallInsts[0], FetchInputTupleSpecialized);
  createNewCall(CallInsts[1], FetchInputTupleSpecialized);
  createNewCall(CallInsts[2], FetchInputTupleSpecialized);
  createNewCall(CallInsts[3], FetchInputTupleSpecialized);

  if(verifyFunction(*SpecializedFunc, &errs())) {
    errs() << "Error specializing function ExecAgg\n";
    errs() << *SpecializedFunc;
    return nullptr;
  }

  return SpecializedFunc;
}

static Function* patchExecNestLoop(std::unique_ptr<Module> &Owner, PlanState *pstate)
{
  Function *SpecializedFunc = cloneFunc(Owner, "ExecNestLoop");

  CallInst *COuter = nullptr, *CInner = nullptr;

  std::vector<CallInst*> CallInsts = findCallsToFunc(Owner, SpecializedFunc, "ExecProcNode", 2);
  if (CallInsts.size() != 2)
    return nullptr;

  // We assume that the first call to ExecProcNode corresponds to the outer plan, and the second is to the inner plan.
  // However, code motion could change this. We need a more robust way of getting the right call.
  COuter = CallInsts[0];
  CInner = CallInsts[1];

  NestLoopState *node = castNode(NestLoopState, pstate);
  PlanState *outerPlan = outerPlanState(node);
  PlanState *innerPlan = innerPlanState(node);

  // patch Inner Plan
  Function *NewExecProcNodeOuter = patchExecProcNode(Owner, outerPlan);
  if (!NewExecProcNodeOuter)
    return nullptr;
  createNewCall(COuter, NewExecProcNodeOuter);

  // patch Outer Plan
  Function *NewExecProcNodeInner = patchExecProcNode(Owner, innerPlan);
  if (!NewExecProcNodeInner)
    return nullptr;
  createNewCall(CInner, NewExecProcNodeInner);

  if(verifyFunction(*SpecializedFunc, &errs())) {
    errs() << "Error specializing function ExecNestLoop\n";
    errs() << *SpecializedFunc;
    return nullptr;
  }

  return SpecializedFunc;
}

static Function* patchExecHashJoin(std::unique_ptr<Module> &Owner, PlanState *pstate)
{
  Function *SpecializedFunc = cloneFunc(Owner, "ExecHashJoin");

  std::vector<CallInst*> CallInstsOuter = findCallsToFunc(Owner, SpecializedFunc, "ExecProcNode", 3);
  if (CallInstsOuter.size() != 3)
    return nullptr;

  std::vector<CallInst*> CallInstsInner = findCallsToFunc(Owner, SpecializedFunc, "MultiExecProcNode");
  if (CallInstsInner.size() != 1)
    return nullptr;

  HashJoinState *node = castNode(HashJoinState, pstate);
  PlanState *outerPlan = outerPlanState(node);
  PlanState *innerPlan = innerPlanState(node);

  // patch Outer Plan
  Function *NewExecProcNodeOuter = patchExecProcNode(Owner, outerPlan);
  if (!NewExecProcNodeOuter)
    return nullptr;
  createNewCall(CallInstsOuter[0], NewExecProcNodeOuter);
  createNewCall(CallInstsOuter[1], NewExecProcNodeOuter);
  createNewCall(CallInstsOuter[2], NewExecProcNodeOuter);

  // patch Inner (Hash) Plan
  Function *NewMultiExecProcNodeInner = patchMultiExecProcNode(Owner, innerPlan);
  if (!NewMultiExecProcNodeInner)
    return nullptr;
  createNewCall(CallInstsInner[0], NewMultiExecProcNodeInner);

  if(verifyFunction(*SpecializedFunc, &errs())) {
    errs() << "Error specializing function ExecHashJoin\n";
    errs() << *SpecializedFunc;
    return nullptr;
  }

  return SpecializedFunc;
}

static Function* patchExecMaterial(std::unique_ptr<Module> &Owner, PlanState *pstate)
{
  Function *SpecializedFunc = cloneFunc(Owner, "ExecMaterial");

  std::vector<CallInst*> CallInsts = findCallsToFunc(Owner, SpecializedFunc, "ExecProcNode");
  if (CallInsts.size() != 1)
    return nullptr;

  CallInst *C = CallInsts[0];

  MaterialState *node = castNode(MaterialState, pstate);
  PlanState *outerPlan = outerPlanState(node);

  Function *NewExecProcNodeOuter = patchExecProcNode(Owner, outerPlan);
  if (!NewExecProcNodeOuter)
    return nullptr;
  createNewCall(C, NewExecProcNodeOuter);

  if(verifyFunction(*SpecializedFunc, &errs())) {
    errs() << "Error specializing function ExecMaterial\n";
    errs() << *SpecializedFunc;
    return nullptr;
  }

  return SpecializedFunc;
}

static Function *patchMultiExecHash(std::unique_ptr<Module> &Owner, PlanState *pstate)
{
  Function *SpecializedFunc = cloneFunc(Owner, "MultiExecHash");

  std::vector<CallInst*> CallInsts = findCallsToFunc(Owner, SpecializedFunc, "ExecProcNode");
  if (CallInsts.size() != 1)
    return nullptr;

  CallInst *C = CallInsts[0];

  HashState *node = castNode(HashState, pstate);
  PlanState *outerPlan = outerPlanState(node);

  Function *NewExecProcNodeOuter = patchExecProcNode(Owner, outerPlan);
  if (!NewExecProcNodeOuter)
    return nullptr;
  createNewCall(C, NewExecProcNodeOuter);

  if(verifyFunction(*SpecializedFunc, &errs())) {
    errs() << "Error specializing function MultiExecHash\n";
    errs() << *SpecializedFunc;
    return nullptr;
  }

  return SpecializedFunc;
}

static Function *patchMultiExecBitmapIndexScan(std::unique_ptr<Module> &Owner, PlanState *pstate)
{
  return cloneFunc(Owner, "MultiExecBitmapIndexScan");
}

static Function *patchExecIndexScan(std::unique_ptr<Module> &Owner, PlanState *pstate)
{
  return cloneFunc(Owner, "ExecIndexScan");
}

static Function *patchExecIndexOnlyScan(std::unique_ptr<Module> &Owner, PlanState *pstate)
{
  return cloneFunc(Owner, "ExecIndexOnlyScan");
}

static Function *patchExecSubqueryScan(std::unique_ptr<Module> &Owner, PlanState *pstate)
{
  return cloneFunc(Owner, "ExecSubqueryScan");
}

static Function *patchExecSeqScan(std::unique_ptr<Module> &Owner, PlanState *pstate)
{
  return cloneFunc(Owner, "ExecSeqScan");
}

static Function *patchExecSort(std::unique_ptr<Module> &Owner, PlanState *pstate)
{
  Function *SpecializedFunc = cloneFunc(Owner, "ExecSort");

  std::vector<CallInst*> CallInsts = findCallsToFunc(Owner, SpecializedFunc, "ExecProcNode");
  if (CallInsts.size() != 1)
    return nullptr;

  CallInst *C = CallInsts[0];

  SortState *node = castNode(SortState, pstate);
  PlanState *outerPlan = outerPlanState(node);

  Function *NewExecProcNodeOuter = patchExecProcNode(Owner, outerPlan);
  if (!NewExecProcNodeOuter)
    return nullptr;
  createNewCall(C, NewExecProcNodeOuter);

  if(verifyFunction(*SpecializedFunc, &errs())) {
    errs() << "Error specializing function ExecSort\n";
    errs() << *SpecializedFunc;
    return nullptr;
  }

  return SpecializedFunc;
}

static Function *patchGatherGetnext(std::unique_ptr<Module> &Owner, PlanState *pstate)
{
  Function *SpecializedFunc = cloneFunc(Owner, "gather_getnext");

  std::vector<CallInst*> CallInsts = findCallsToFunc(Owner, SpecializedFunc, "ExecProcNode");
  if (CallInsts.size() != 1)
    return nullptr;

  CallInst *C = CallInsts[0];

  GatherState *node = castNode(GatherState, pstate);
  PlanState *outerPlan = outerPlanState(node);

  Function *NewExecProcNodeOuter = patchExecProcNode(Owner, outerPlan);
  if (!NewExecProcNodeOuter)
    return nullptr;
  createNewCall(C, NewExecProcNodeOuter);

  if(verifyFunction(*SpecializedFunc, &errs())) {
    errs() << "Error specializing function gather_getnext\n";
    errs() << *SpecializedFunc;
    return nullptr;
  }

  return SpecializedFunc;
}

static Function *patchExecGather(std::unique_ptr<Module> &Owner, PlanState *pstate)
{
  Function *SpecializedFunc = cloneFunc(Owner, "ExecGather");

  std::vector<CallInst*> CallInsts = findCallsToFunc(Owner, SpecializedFunc, "gather_getnext");
  if (CallInsts.size() != 1)
    return nullptr;

  CallInst *C = CallInsts[0];

  Function *GatherGetnextSpecialized = patchGatherGetnext(Owner, pstate);
  if (!GatherGetnextSpecialized)
    return nullptr;
  createNewCall(C, GatherGetnextSpecialized);

  if(verifyFunction(*SpecializedFunc, &errs())) {
    errs() << "Error specializing function ExecGather\n";
    errs() << *SpecializedFunc;
    return nullptr;
  }

  return SpecializedFunc;
}

static Function *patchGatherMergeReadnext(std::unique_ptr<Module> &Owner, PlanState *pstate)
{
  Function *SpecializedFunc = cloneFunc(Owner, "gather_merge_readnext");

  std::vector<CallInst*> CallInsts = findCallsToFunc(Owner, SpecializedFunc, "ExecProcNode");
  if (CallInsts.size() != 1)
    return nullptr;

  CallInst *C = CallInsts[0];

  GatherState *node = castNode(GatherState, pstate);
  PlanState *outerPlan = outerPlanState(node);

  Function *NewExecProcNodeOuter = patchExecProcNode(Owner, outerPlan);
  if (!NewExecProcNodeOuter)
    return nullptr;
  createNewCall(C, NewExecProcNodeOuter);

  if(verifyFunction(*SpecializedFunc, &errs())) {
    errs() << "Error specializing function gather_merge_readnext\n";
    errs() << *SpecializedFunc;
    return nullptr;
  }

  return SpecializedFunc;
}

static Function *patchExecGatherMerge(std::unique_ptr<Module> &Owner, PlanState *pstate)
{
  Function *SpecializedFunc = cloneFunc(Owner, "ExecGatherMerge");

  std::vector<CallInst*> CallInsts = findCallsToFunc(Owner, SpecializedFunc, "gather_merge_readnext", 2);
  if (CallInsts.size() != 2)
    return nullptr;

  Function *GatherGetnextSpecialized = patchGatherMergeReadnext(Owner, pstate);
  if (!GatherGetnextSpecialized)
    return nullptr;
  createNewCall(CallInsts[0], GatherGetnextSpecialized);
  createNewCall(CallInsts[1], GatherGetnextSpecialized);

  if(verifyFunction(*SpecializedFunc, &errs())) {
    errs() << "Error specializing function ExecGatherMerge\n";
    errs() << *SpecializedFunc;
    return nullptr;
  }

  return SpecializedFunc;
}

static Function* patchBitmapHeapNext(std::unique_ptr<Module> &Owner, PlanState *pstate)
{
  Function *SpecializedFunc = cloneFunc(Owner, "BitmapHeapNext");

  std::vector<CallInst*> CallInstsOuter = findCallsToFunc(Owner, SpecializedFunc, "MultiExecProcNode", 2);
  if (CallInstsOuter.size() != 2)
    return nullptr;

  BitmapHeapScanState *node = castNode(BitmapHeapScanState, pstate);
  PlanState *outerPlan = outerPlanState(node);

  // patch Outer (Hash) Plan
  Function *NewMultiExecProcNodeOuter = patchMultiExecProcNode(Owner, outerPlan);
  if (!NewMultiExecProcNodeOuter)
    return nullptr;
  createNewCall(CallInstsOuter[0], NewMultiExecProcNodeOuter);
  createNewCall(CallInstsOuter[1], NewMultiExecProcNodeOuter);

  if(verifyFunction(*SpecializedFunc, &errs())) {
    errs() << "Error specializing function ExecHashJoin\n";
    errs() << *SpecializedFunc;
    return nullptr;
  }

  return SpecializedFunc;
}

static Function *patchExecScanFetch(std::unique_ptr<Module> &Owner, PlanState *pstate)
{
  Function *SpecializedFunc = cloneFunc(Owner, "ExecScanFetch");

  /* We are trying to find this call:
     %59 = tail call %struct.TupleTableSlot* %1(%struct.ScanState* nonnull %0) #11 */
  PointerType *TTS = Owner->getTypeByName("struct.TupleTableSlot")->getPointerTo();
  PointerType *SS = Owner->getTypeByName("struct.ScanState")->getPointerTo();
  if (!TTS || !SS) {
    errs() << "Could not find TTS (" << TTS << ") or SS (" << SS << ")\n";
    return {};
  }

  CallInst *C = nullptr;
  for (inst_iterator I = inst_begin(SpecializedFunc), E = inst_end(SpecializedFunc); I != E; I++) {
    if (CallInst *CC = dyn_cast<CallInst>(&*I)) {
      /* it is an indirect call, so getCalledFunction() should be null */
      if (CC->getCalledFunction() == nullptr) {
        FunctionType *FT = CC->getFunctionType();
        if (FT->getReturnType() != TTS)
          continue;
        if (FT->getNumParams() != 1 || FT->getParamType(0) != SS)
          continue;
        C = CC;
        break;
      }
    }
  }
  if (!C) {
    errs() << "Could not find call on ExecScanFetch\n";
    return nullptr;
  }

  Function *NewFunc = nullptr;
  Type *CastToTy = nullptr;
  switch(nodeTag(pstate)) {
    case T_BitmapHeapScanState:
      NewFunc = patchBitmapHeapNext(Owner, pstate);
      CastToTy = Owner->getTypeByName("struct.BitmapHeapScanState")->getPointerTo();
      break;
    default:
      errs() << "not yet supported\n";
  }

  if (!NewFunc)
    return nullptr;
  createNewCallCast(C, NewFunc, CastToTy);

  if(verifyFunction(*SpecializedFunc, &errs())) {
    errs() << "Error specializing function ExecScanFetch\n";
    errs() << *SpecializedFunc;
    return nullptr;
  }

  return SpecializedFunc;
}

static Function *patchExecScan(std::unique_ptr<Module> &Owner, PlanState *pstate)
{
  Function *SpecializedFunc = cloneFunc(Owner, "ExecScan");

  std::vector<CallInst*> CallInsts = findCallsToFunc(Owner, SpecializedFunc, "ExecScanFetch", 3);
  if (CallInsts.size() != 3)
    return nullptr;

  Function *ExecScanFetchSpecialized = patchExecScanFetch(Owner, pstate);
  if (!ExecScanFetchSpecialized)
    return nullptr;
  createNewCall(CallInsts[0], ExecScanFetchSpecialized);
  createNewCall(CallInsts[1], ExecScanFetchSpecialized);
  createNewCall(CallInsts[2], ExecScanFetchSpecialized);

  if(verifyFunction(*SpecializedFunc, &errs())) {
    errs() << "Error specializing function ExecScan\n";
    errs() << *SpecializedFunc;
    return nullptr;
  }

  return SpecializedFunc;
}

static Function *patchExecBitmapHeapScan(std::unique_ptr<Module> &Owner, PlanState *pstate)
{
  Function *SpecializedFunc = cloneFunc(Owner, "ExecBitmapHeapScan");

  std::vector<CallInst*> CallInsts = findCallsToFunc(Owner, SpecializedFunc, "ExecScan");
  if (CallInsts.size() != 1)
    return nullptr;

  Function *ExecScanSpecialized = patchExecScan(Owner, pstate);
  if (!ExecScanSpecialized)
    return nullptr;
  createNewCall(CallInsts[0], ExecScanSpecialized);

  if(verifyFunction(*SpecializedFunc, &errs())) {
    errs() << "Error specializing function ExecBitmapHeapScan\n";
    errs() << *SpecializedFunc;
    return nullptr;
  }

  return SpecializedFunc;
}

static Function* patchExecMergeJoin(std::unique_ptr<Module> &Owner, PlanState *pstate)
{
  Function *SpecializedFunc = cloneFunc(Owner, "ExecMergeJoin");

  std::vector<CallInst*> CallInsts = findCallsToFunc(Owner, SpecializedFunc, "ExecProcNode", 8);
  if (CallInsts.size() != 8)
    return nullptr;

  // Inspecting the IR, we saw that calls 1,4,5 and 8 are to the outerPlan and calls 2,3,6 and 7 are to the innerPlan
  // TODO: We need a more robust way of getting the right call.
  std::vector<CallInst*> COuter = {CallInsts[0], CallInsts[3], CallInsts[4], CallInsts[7]};
  std::vector<CallInst*> CInner = {CallInsts[1], CallInsts[2], CallInsts[5], CallInsts[6]};

  MergeJoinState *node = castNode(MergeJoinState, pstate);
  PlanState *outerPlan = outerPlanState(node);
  PlanState *innerPlan = innerPlanState(node);

  // patch Inner Plan
  Function *NewExecProcNodeOuter = patchExecProcNode(Owner, outerPlan);
  if (!NewExecProcNodeOuter)
    return nullptr;
  createNewCall(COuter[0], NewExecProcNodeOuter);
  createNewCall(COuter[1], NewExecProcNodeOuter);
  createNewCall(COuter[2], NewExecProcNodeOuter);
  createNewCall(COuter[3], NewExecProcNodeOuter);

  // patch Outer Plan
  Function *NewExecProcNodeInner = patchExecProcNode(Owner, innerPlan);
  if (!NewExecProcNodeInner)
    return nullptr;
  createNewCall(CInner[0], NewExecProcNodeInner);
  createNewCall(CInner[1], NewExecProcNodeInner);
  createNewCall(CInner[2], NewExecProcNodeInner);
  createNewCall(CInner[3], NewExecProcNodeInner);

  if(verifyFunction(*SpecializedFunc, &errs())) {
    errs() << "Error specializing function ExecMergeJoin\n";
    errs() << *SpecializedFunc;
    return nullptr;
  }

  return SpecializedFunc;
}

static Function* patchExecResult(std::unique_ptr<Module> &Owner, PlanState *pstate)
{
  Function *SpecializedFunc = cloneFunc(Owner, "ExecResult");

  std::vector<CallInst*> CallInsts = findCallsToFunc(Owner, SpecializedFunc, "ExecProcNode");
  if (CallInsts.size() != 1)
    return nullptr;

  CallInst *C = CallInsts[0];

  if (pstate->lefttree) {
    Function *NewExecProcNodeOuter = patchExecProcNode(Owner, pstate->lefttree);
    if (!NewExecProcNodeOuter)
      return nullptr;
    createNewCall(C, NewExecProcNodeOuter);
  }

  if(verifyFunction(*SpecializedFunc, &errs())) {
    errs() << "Error specializing function ExecResult\n";
    errs() << *SpecializedFunc;
    return nullptr;
  }

  return SpecializedFunc;
}

static Function* patchExecLimit(std::unique_ptr<Module> &Owner, PlanState *pstate)
{
  Function *SpecializedFunc = cloneFunc(Owner, "ExecLimit");

  std::vector<CallInst*> CallInsts = findCallsToFunc(Owner, SpecializedFunc, "ExecProcNode", 4);
  if (CallInsts.size() != 4)
    return nullptr;

  Function *NewExecProcNode = patchExecProcNode(Owner, pstate->lefttree);
  if (!NewExecProcNode)
    return nullptr;

  createNewCall(CallInsts[0], NewExecProcNode);
  createNewCall(CallInsts[1], NewExecProcNode);
  createNewCall(CallInsts[2], NewExecProcNode);
  createNewCall(CallInsts[3], NewExecProcNode);

  if(verifyFunction(*SpecializedFunc, &errs())) {
    errs() << "Error specializing function ExecLimit\n";
    errs() << *SpecializedFunc;
    return nullptr;
  }

  return SpecializedFunc;
}

static Function* patchExecProcNode(std::unique_ptr<Module> &Owner, PlanState *node)
{
  Function *NewFunc = nullptr;
  //FIXME: We are actually skipping a call to ExecProcNodeFirst. This should be safe for most cases...
  if (node->ExecProcNodeReal == ExecSeqScan)
    NewFunc = patchExecSeqScan(Owner, node);
  else if (node->ExecProcNodeReal == ExecIndexScan)
    NewFunc = patchExecIndexScan(Owner, node);
  else if (node->ExecProcNodeReal == ExecIndexOnlyScan)
    NewFunc = patchExecIndexOnlyScan(Owner, node);
  else if (node->ExecProcNodeReal == ExecSubqueryScan)
    NewFunc = patchExecSubqueryScan(Owner, node);
  else if (node->ExecProcNodeReal == ExecBitmapHeapScan)
    NewFunc = patchExecBitmapHeapScan(Owner, node);
  else if (node->ExecProcNodeReal == ExecNestLoop)
    NewFunc = patchExecNestLoop(Owner, node);
  else if (node->ExecProcNodeReal == ExecAgg)
    NewFunc = patchExecAgg(Owner, node);
  else if (node->ExecProcNodeReal == ExecMaterial)
    NewFunc = patchExecMaterial(Owner, node);
  else if (node->ExecProcNodeReal == ExecHashJoin)
    NewFunc = patchExecHashJoin(Owner, node);
  else if (node->ExecProcNodeReal == ExecSort)
    NewFunc = patchExecSort(Owner, node);
  else if (node->ExecProcNodeReal == ExecGather)
    NewFunc = patchExecGather(Owner, node);
  else if (node->ExecProcNodeReal == ExecGatherMerge)
    NewFunc = patchExecGatherMerge(Owner, node);
  else if (node->ExecProcNodeReal == ExecMergeJoin)
    NewFunc = patchExecMergeJoin(Owner, node);
  else if (node->ExecProcNodeReal == ExecLimit)
    NewFunc = patchExecLimit(Owner, node);
  else if (node->ExecProcNodeReal == ExecResult)
    NewFunc = patchExecResult(Owner, node);

  if (!NewFunc)
    errs() << "Failed to patch for " << node << "\n";

  return NewFunc;
}

static Function* patchMultiExecProcNode(std::unique_ptr<Module> &Owner, PlanState *node)
{
  Function *SpecializedFunc = cloneFunc(Owner, "MultiExecProcNode");

  // MultiExecProcNode chooses which function to call through a switch on nodeTag(node).
  // Since we know at JIT-time the value of nodeTag(node), we can replace the switch condition for a constant.
  // Further optimizations should ensure that the switch gets eliminated.
  for (inst_iterator I = inst_begin(SpecializedFunc), E = inst_end(SpecializedFunc); I != E; I++) {
    if (SwitchInst *S = dyn_cast<SwitchInst>(&*I)) {
      llvm::Value *Condition = S->getCondition();
      int numBits = (dyn_cast<IntegerType>(Condition->getType()))->getBitWidth();
      ConstantInt *C = ConstantInt::get(IntegerType::get(Owner->getContext(), numBits), nodeTag(node));
      Condition->replaceAllUsesWith(C);
      break;
    }
  }

  std::vector<CallInst*> CallInsts;
  Function *NewMultiExec;

  switch (nodeTag(node))
  {
    case T_HashState:
      CallInsts = findCallsToFunc(Owner, SpecializedFunc, "MultiExecHash");
      NewMultiExec = patchMultiExecHash(Owner, node);
      break;
    case T_BitmapIndexScanState:
      CallInsts = findCallsToFunc(Owner, SpecializedFunc, "MultiExecBitmapIndexScan");
      NewMultiExec = patchMultiExecBitmapIndexScan(Owner, node);
      break;
    case T_BitmapAndState:
    case T_BitmapOrState:
      errs() << "[WARNING] patchMultiExecProcNode: node not yet implemented: " << nodeTag(node) << "\n";
    default:
      CallInsts = {};
  }

  if (CallInsts.size() != 1 || !NewMultiExec)
    return nullptr;

  createNewCall(CallInsts[0], NewMultiExec);

  if(verifyFunction(*SpecializedFunc, &errs())) {
    errs() << "Error specializing function MultiExecProcNode\n";
    errs() << *SpecializedFunc;
    return nullptr;
  }

  return SpecializedFunc;
}

/* Return true if no errors */
static Function* patchExecutePlan(std::unique_ptr<Module> &Owner, PlanState *node)
{
  Function *SpecializedFunc = cloneFunc(Owner, "ExecutePlan");
  SpecializedFunc->setName("ExecutePlanJIT");

  std::vector<CallInst*> CallInsts = findCallsToFunc(Owner, SpecializedFunc, "ExecProcNode");
  if (CallInsts.size() != 1)
    return nullptr;

  Function *Patched_ExecProcNode = patchExecProcNode(Owner, node);
  if (!Patched_ExecProcNode)
    return nullptr;

  CallInst *C = CallInsts[0];
  createNewCall(C, Patched_ExecProcNode);

  if(verifyFunction(*SpecializedFunc, &errs())) {
    errs() << "Error specializing function ExecutePlanJIT\n";
    errs() << *SpecializedFunc;
    return nullptr;
  }

  return SpecializedFunc;
}

void traverse(PlanState *node)
{
  if (!node)
    return;
  errs() << nodeTag(node) << " " << node << "\n";
  traverse(node->lefttree);
  traverse(node->righttree);
}

//extern int lli_main(int argc, char **argv, llvm::ArrayRef<GenericValue> ArgValues);
extern int opt_main(std::unique_ptr<Module> &M);

extern "C" bool ExecuteCompiledPlan(EState *estate,
                                    PlanState *planstate,
                                    bool use_parallel_mode,
                                    CmdType operation,
                                    bool sendTuples,
                                    uint64 numberTuples,
                                    ScanDirection direction,
                                    DestReceiver *dest,
                                    bool execute_once)
{
  //mtx.lock();
  //errs() << "\n\n--------------\n\n";
  //traverse(planstate);
  //errs() << "\n--------------\n\n";
  //mtx.unlock();
  char *opt_argv[] = {"opt", "/local/data/ichilevi/git_repos/postgresql-10.0/build/src/backend/postgre.ll"};
  LLVMContext TheContext;
  InitializeNativeTarget();
  InitializeNativeTargetAsmPrinter();
  InitializeNativeTargetAsmParser();

  SMDiagnostic error;
  std::unique_ptr<Module> Owner = parseIRFile("/local/data/ichilevi/git_repos/postgresql-10.0/build/src/backend/postgre.ll", error, TheContext);
  if (!Owner) {
    errs() << error.getMessage();
    errs() << "Could not load pre-compiled file\n";
    return false;
  }

  instr_time start, duration;
  INSTR_TIME_SET_CURRENT(start);
  if (!patchExecutePlan(Owner, planstate)) {
    errs() << "[ERROR] could not patch execute plan\n";
    return false;
  }
  INSTR_TIME_SET_CURRENT(duration);
  INSTR_TIME_SUBTRACT(duration, start);
  double patch_duration = INSTR_TIME_GET_MILLISEC(duration);
  errs() << "Took " << patch_duration << " to patch\n";

  for (auto &F : *Owner) {
    if (F.getName().find("JIT") != std::string::npos) {
      errs() << *&F << "\n";
    }
  }

  INSTR_TIME_SET_CURRENT(start);
  opt_main(Owner);
  INSTR_TIME_SET_CURRENT(duration);
  INSTR_TIME_SUBTRACT(duration, start);
  double optimize_duration = INSTR_TIME_GET_MILLISEC(duration);
  errs() << "Took " << optimize_duration << " to optimize\n";

  /*
  mtx.lock();
  for (auto &F : *Owner) {
    if (F.getName().find("JIT") != std::string::npos)
      printFunc(&F);
  }
  mtx.unlock();
  */

  //INSTR_TIME_SET_CURRENT(start);
  static std::unique_ptr<KaleidoscopeJIT> TheJIT = llvm::make_unique<KaleidoscopeJIT>();
  auto H = TheJIT->addModule(std::move(Owner));
  //INSTR_TIME_SET_CURRENT(duration);
  //INSTR_TIME_SUBTRACT(duration, start);
  //errs() << "Generating module: " << INSTR_TIME_GET_MILLISEC(duration) << "\n";

  INSTR_TIME_SET_CURRENT(start);
  auto ExprSymbol = TheJIT->findSymbol("ExecutePlanJIT");
  //auto ExprSymbol = TheJIT->findSymbol("ExecSeqScanJIT");
  if (!ExprSymbol) {
    errs() << "[ERROR] Could not find symbol\n";
    return false;
  }
  INSTR_TIME_SET_CURRENT(duration);
  INSTR_TIME_SUBTRACT(duration, start);
  double find_symbol_duration = INSTR_TIME_GET_MILLISEC(duration);
  errs() << "Took " << find_symbol_duration << " to find symbol\n";

  void (*execPlan) (EState*, PlanState*, bool, CmdType, bool, uint64, ScanDirection, DestReceiver*, bool) =
    (void (*) (EState*, PlanState*, bool, CmdType, bool, uint64, ScanDirection, DestReceiver*, bool)) cantFail(ExprSymbol.getAddress());

  execPlan(estate, planstate, use_parallel_mode, operation, sendTuples, numberTuples, direction, dest, execute_once);

  TheJIT->removeModule(H);

  errs() << "JIT executed successfully\n";
  return true;
}

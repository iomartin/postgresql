//===- opt.cpp - The LLVM Modular Optimizer -------------------------------===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
//
// Optimizations may be specified an arbitrary number of times on the command
// line, They are run in the order specified.
//
//===----------------------------------------------------------------------===//

#include "executor/BreakpointPrinter.h"
#include "executor/NewPMDriver.h"
#include "executor/PassPrinters.h"
#include "llvm/ADT/Triple.h"
#include "llvm/Analysis/CallGraph.h"
#include "llvm/Analysis/CallGraphSCCPass.h"
#include "llvm/Analysis/LoopPass.h"
#include "llvm/Analysis/RegionPass.h"
#include "llvm/Analysis/TargetLibraryInfo.h"
#include "llvm/Analysis/TargetTransformInfo.h"
#include "llvm/Bitcode/BitcodeWriterPass.h"
#include "llvm/CodeGen/CommandFlags.h"
#include "llvm/CodeGen/TargetPassConfig.h"
#include "llvm/IR/DataLayout.h"
#include "llvm/IR/DebugInfo.h"
#include "llvm/IR/IRPrintingPasses.h"
#include "llvm/IR/LLVMContext.h"
#include "llvm/IR/LegacyPassManager.h"
#include "llvm/IR/LegacyPassNameParser.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/Verifier.h"
#include "llvm/IRReader/IRReader.h"
#include "llvm/InitializePasses.h"
#include "llvm/LinkAllIR.h"
#include "llvm/LinkAllPasses.h"
#include "llvm/MC/SubtargetFeature.h"
#include "llvm/Support/Debug.h"
#include "llvm/Support/FileSystem.h"
#include "llvm/Support/Host.h"
#include "llvm/Support/ManagedStatic.h"
#include "llvm/Support/PluginLoader.h"
#include "llvm/Support/PrettyStackTrace.h"
#include "llvm/Support/Signals.h"
#include "llvm/Support/SourceMgr.h"
#include "llvm/Support/SystemUtils.h"
#include "llvm/Support/TargetRegistry.h"
#include "llvm/Support/TargetSelect.h"
#include "llvm/Support/ToolOutputFile.h"
#include "llvm/Support/YAMLTraits.h"
#include "llvm/Target/TargetMachine.h"
#include "llvm/Transforms/Coroutines.h"
#include "llvm/Transforms/IPO/AlwaysInliner.h"
#include "llvm/Transforms/IPO/PassManagerBuilder.h"
#include "llvm/Transforms/Utils/Cloning.h"
#include <algorithm>
#include <memory>
using namespace llvm;
using namespace opt_tool;

static inline void addPass(legacy::PassManagerBase &PM, Pass *P) {
  // Add the pass to the pass manager...
  PM.add(P);
}

/// This routine adds optimization passes based on selected optimization level,
/// OptLevel.
///
/// OptLevel - Optimization Level
static void AddOptimizationPasses(legacy::PassManagerBase &MPM,
                                  legacy::FunctionPassManager &FPM,
                                  unsigned OptLevel, unsigned SizeLevel) {
  //FPM.add(createVerifierPass()); // Verify that input is correct

  PassManagerBuilder Builder;

  Builder.OptLevel = 3;
  Builder.populateFunctionPassManager(FPM);
  //Builder.populateModulePassManager2(MPM);

  MPM.add(createDeadArgEliminationPass());
  bool ExpensiveCombines = OptLevel > 2;
  MPM.add(createInstructionCombiningPass(ExpensiveCombines));
  MPM.add(createPruneEHPass()); // Remove dead EH info
  MPM.add(createFunctionInliningPass(OptLevel, SizeLevel, false));
  //MPM.add(createFunctionInliningPass(510)); // default is 255
}

static void AddStandardLinkPasses(legacy::PassManagerBase &PM) {
  PassManagerBuilder Builder;
  Builder.VerifyInput = true;

  Builder.Inliner = createFunctionInliningPass();
  Builder.populateLTOPassManager(PM);
}

//===----------------------------------------------------------------------===//
// main for opt
//
int opt_main(std::unique_ptr<Module> &M) {

  // Enable debug stream buffering.
  EnableDebugBuffering = true;

//  llvm_shutdown_obj Y;  // Call llvm_shutdown() on exit.
  LLVMContext Context;

  //InitializeAllTargets();
  //InitializeAllTargetMCs();
  //InitializeAllAsmPrinters();
  //InitializeAllAsmParsers();

  // Initialize passes
  PassRegistry &Registry = *PassRegistry::getPassRegistry();
  initializeCore(Registry);
  initializeCoroutines(Registry);
  initializeScalarOpts(Registry);
  initializeObjCARCOpts(Registry);
  initializeVectorization(Registry);
  initializeIPO(Registry);
  initializeAnalysis(Registry);
  initializeTransformUtils(Registry);
  initializeInstCombine(Registry);
  initializeInstrumentation(Registry);
  initializeTarget(Registry);
  // For codegen passes, only passes that do IR to IR transformation are
  // supported.
  initializeScalarizeMaskedMemIntrinPass(Registry);
  initializeCodeGenPreparePass(Registry);
  initializeAtomicExpandPass(Registry);
  initializeRewriteSymbolsLegacyPassPass(Registry);
  initializeWinEHPreparePass(Registry);
  initializeDwarfEHPreparePass(Registry);
  initializeSafeStackLegacyPassPass(Registry);
  initializeSjLjEHPreparePass(Registry);
  initializePreISelIntrinsicLoweringLegacyPassPass(Registry);
  initializeGlobalMergePass(Registry);
  initializeInterleavedAccessPass(Registry);
  initializeCountingFunctionInserterPass(Registry);
  initializeUnreachableBlockElimLegacyPassPass(Registry);
  initializeExpandReductionsPass(Registry);
  initializeWriteBitcodePassPass(Registry);

//  if (AnalyzeOnly && NoOutput) {
//    errs() << "opt: analyze mode conflicts with no-output mode.\n";
//    return 1;
//  }
//
//  SMDiagnostic Err;
//
//  Context.setDiscardValueNames(DiscardValueNames);
//  if (!DisableDITypeMap)
//    Context.enableDebugTypeODRUniquing();
//
//  if (PassRemarksWithHotness)
//    Context.setDiagnosticsHotnessRequested(true);
//
//  if (PassRemarksHotnessThreshold)
//    Context.setDiagnosticsHotnessThreshold(PassRemarksHotnessThreshold);
//
//  std::unique_ptr<ToolOutputFile> OptRemarkFile;
//  if (RemarksFilename != "") {
//    std::error_code EC;
//    OptRemarkFile =
//        llvm::make_unique<ToolOutputFile>(RemarksFilename, EC, sys::fs::F_None);
//    if (EC) {
//      errs() << EC.message() << '\n';
//      return 1;
//    }
//    Context.setDiagnosticsOutputFile(
//        llvm::make_unique<yaml::Output>(OptRemarkFile->os()));
//  }
//
//  // Strip debug info before running the verifier.
//  if (StripDebug)
//    StripDebugInfo(*M);
//
//  // Immediately run the verifier to catch any problems before starting up the
//  // pass pipelines.  Otherwise we can crash on broken code during
//  // doInitialization().
//  if (!NoVerify && verifyModule(*M, &errs())) {
//    errs() << "opt: " << InputFilename
//           << ": error: input module is broken!\n";
//    return 1;
//  }
//
//  // If we are supposed to override the target triple or data layout, do so now.
//  if (!TargetTriple.empty())
//    M->setTargetTriple(Triple::normalize(TargetTriple));
//  if (!ClDataLayout.empty())
//    M->setDataLayout(ClDataLayout);
//
//  Triple ModuleTriple(M->getTargetTriple());
//  std::string CPUStr, FeaturesStr;
//  TargetMachine *Machine = nullptr;
//  const TargetOptions Options = InitTargetOptionsFromCodeGenFlags();
//
//  if (ModuleTriple.getArch()) {
//    CPUStr = getCPUStr();
//    FeaturesStr = getFeaturesStr();
//    Machine = GetTargetMachine(ModuleTriple, CPUStr, FeaturesStr, Options);
//  }
//
//  std::unique_ptr<TargetMachine> TM(Machine);
//
//  // Override function attributes based on CPUStr, FeaturesStr, and command line
//  // flags.
//  setFunctionAttributes(CPUStr, FeaturesStr, *M);
//
//  // If the output is set to be emitted to standard out, and standard out is a
//  // console, print out a warning message and refuse to do it.  We don't
//  // impress anyone by spewing tons of binary goo to a terminal.
//  if (!Force && !NoOutput && !AnalyzeOnly && !OutputAssembly)
//    if (CheckBitcodeOutputToConsole(Out->os(), !Quiet))
//      NoOutput = true;

  // Create a PassManager to hold and optimize the collection of passes we are
  // about to build.
  //
  legacy::PassManager Passes;

  // Add an appropriate TargetLibraryInfo pass for the module's triple.
//  TargetLibraryInfoImpl TLII(ModuleTriple);
//
//  // The -disable-simplify-libcalls flag actually disables all builtin optzns.
//  if (DisableSimplifyLibCalls)
//    TLII.disableAllFunctions();
//  Passes.add(new TargetLibraryInfoWrapperPass(TLII));
//
//  // Add internal analysis passes from the target machine.
//  Passes.add(createTargetTransformInfoWrapperPass(TM ? TM->getTargetIRAnalysis()
//                                                     : TargetIRAnalysis()));
//
  std::unique_ptr<legacy::FunctionPassManager> FPasses;
  FPasses.reset(new legacy::FunctionPassManager(M.get()));
  FPasses->add(createTargetTransformInfoWrapperPass(TargetIRAnalysis()));
//    FPasses->add(createTargetTransformInfoWrapperPass(
//        TM ? TM->getTargetIRAnalysis() : TargetIRAnalysis()));
//
//  if (PrintBreakpoints) {
//    // Default to standard output.
//    if (!Out) {
//      if (OutputFilename.empty())
//        OutputFilename = "-";
//
//      std::error_code EC;
//      Out = llvm::make_unique<ToolOutputFile>(OutputFilename, EC,
//                                              sys::fs::F_None);
//      if (EC) {
//        errs() << EC.message() << '\n';
//        return 1;
//      }
//    }
//    Passes.add(createBreakpointPrinter(Out->os()));
//    NoOutput = true;
//  }
//
//  if (TM) {
//    // FIXME: We should dyn_cast this when supported.
//    auto &LTM = static_cast<LLVMTargetMachine &>(*TM);
//    Pass *TPC = LTM.createPassConfig(Passes);
//    Passes.add(TPC);
//  }
//
//  // Create a new optimization pass for each one specified on the command line
//
//  AddStandardLinkPasses(Passes);

  AddOptimizationPasses(Passes, *FPasses, 3, 0);

  if (FPasses) {
    FPasses->doInitialization();
    for (Function &F : *M)
      if (F.getName().find("JIT") != std::string::npos) {
        FPasses->run(F);
        //errs() << *&F << "\n\n\n\n\n\n\n";
      }
    FPasses->doFinalization();
  }

//  // Check that the module is well formed on completion of optimization
//  if (!NoVerify && !VerifyEach)
//    Passes.add(createVerifierPass());
//
//  // In run twice mode, we want to make sure the output is bit-by-bit
//  // equivalent if we run the pass manager again, so setup two buffers and
//  // a stream to write to them. Note that llc does something similar and it
//  // may be worth to abstract this out in the future.
//  SmallVector<char, 0> Buffer;
//  SmallVector<char, 0> CompileTwiceBuffer;
//  std::unique_ptr<raw_svector_ostream> BOS;
//  raw_ostream *OS = nullptr;
//
//  // Write bitcode or assembly to the output as the last step...
//  if (!NoOutput && !AnalyzeOnly) {
//    assert(Out);
//    OS = &Out->os();
//    if (RunTwice) {
//      BOS = make_unique<raw_svector_ostream>(Buffer);
//      OS = BOS.get();
//    }
//    if (OutputAssembly) {
//      if (EmitSummaryIndex)
//        report_fatal_error("Text output is incompatible with -module-summary");
//      if (EmitModuleHash)
//        report_fatal_error("Text output is incompatible with -module-hash");
//      Passes.add(createPrintModulePass(*OS, "", PreserveAssemblyUseListOrder));
//    } else if (OutputThinLTOBC)
//      Passes.add(createWriteThinLTOBitcodePass(
//          *OS, ThinLinkOut ? &ThinLinkOut->os() : nullptr));
//    else
//      Passes.add(createBitcodeWriterPass(*OS, PreserveBitcodeUseListOrder,
//                                         EmitSummaryIndex, EmitModuleHash));
//  }
//
//  // Before executing passes, print the final values of the LLVM options.
//  cl::PrintOptionValues();
//
//  // If requested, run all passes again with the same pass manager to catch
//  // bugs caused by persistent state in the passes
//  if (RunTwice) {
//      std::unique_ptr<Module> M2(CloneModule(M.get()));
//      Passes.run(*M2);
//      CompileTwiceBuffer = Buffer;
//      Buffer.clear();
//  }

  Passes.run(*M);

//  // Compare the two outputs and make sure they're the same
//  if (RunTwice) {
//    assert(Out);
//    if (Buffer.size() != CompileTwiceBuffer.size() ||
//        (memcmp(Buffer.data(), CompileTwiceBuffer.data(), Buffer.size()) !=
//         0)) {
//      errs() << "Running the pass manager twice changed the output.\n"
//                "Writing the result of the second run to the specified output.\n"
//                "To generate the one-run comparison binary, just run without\n"
//                "the compile-twice option\n";
//      Out->os() << BOS->str();
//      Out->keep();
//      if (OptRemarkFile)
//        OptRemarkFile->keep();
//      return 1;
//    }
//    Out->os() << BOS->str();
//  }
//
//  // Declare success.
//  if (!NoOutput || PrintBreakpoints)
//    Out->keep();
//
//  if (OptRemarkFile)
//    OptRemarkFile->keep();
//
//  if (ThinLinkOut)
//    ThinLinkOut->keep();

  return 0;
}
